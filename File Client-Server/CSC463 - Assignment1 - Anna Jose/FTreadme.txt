*** READ ME ***
Name:		Anna Marietta Jose
Course:	  	463
Assignment:	 #1

1. Once unzipped, open FTClient.java, FTServer.java and FTThread.java in a compiler.

2. Server:
In FTThread.java, enter a PORTNUMBER to listen for clients.
In FTServer.java, enter the "directory" of the files you want to share.

3. Client:
In FTClient.java, enter the IPADDRESS of the server, and the same PORTNUMBER in FTThread.java.

4. Compile FTThread.java and FTServer.java.

5. Compile FTClient.java.

6. Client:
Start upload and/or download.
Chose exit to end connection.


	 	