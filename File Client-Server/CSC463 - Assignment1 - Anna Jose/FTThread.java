/* Name:  Anna Marietta Jose
 * Course:   463
 * Assignment:   #1
 */

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class FTThread {
  int countConnections = 0;
  int port;
  ServerSocket serverSocket = null;
  Socket socket = null;
  
  public static void main(String[] args) throws IOException {
    System.out.println( "Server has started. Waiting for connections" );
    FTThread thread = new FTThread(/*SERVER PORT NUMBER*/); //Enter Server Port Number
    thread.set();
  }
  
  public FTThread(int port) {
    this.port = port;
  }
  
  public void set() throws IOException {
    serverSocket = new ServerSocket(port);
    while ( true ) {
      try {
        socket = serverSocket.accept();
        countConnections++;
        FTServer server = new FTServer(socket, countConnections, this);
        new Thread(server).start();
      }
      catch (Exception e) {
        System.out.println(e);
      }
    }
  }
}
