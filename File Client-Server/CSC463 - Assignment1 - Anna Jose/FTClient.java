/* Name:  Anna Marietta Jose
 * Course:   463
 * Assignment:   #1
 */

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import java.io.*;
import java.net.*;

public class FTClient {
  public static void main(String[] argv) throws Exception {
    
    Socket clientSocket = null;
    BufferedReader bReader = null;
    OutputStream oStream = null;
    InputStream iStream = null;
    PrintWriter pWriter = null;
    
    /* Enter Server IP address and Port number
     */ 
    clientSocket = new Socket(/*IPADDRESS*/, /*PORTNUMBER*/);
    oStream = clientSocket.getOutputStream();
    iStream = clientSocket.getInputStream();
    bReader = new BufferedReader(new InputStreamReader(iStream));
    
    boolean start = true;
    while (start == true) {
      JFrame frame = new JFrame();
      Object[] options = {"Download",
        "Upload", "Exit"};
      int n = JOptionPane.showOptionDialog(frame,
                                           "Please choose:",
                                           "Upload or Download",
                                           JOptionPane.YES_NO_OPTION,
                                           JOptionPane.QUESTION_MESSAGE,
                                           null, 
                                           options, 
                                           options[0]);
      //Client Download
      if (n==0) {
        String response = null;
        response = bReader.readLine();
        int i=0;
        String none =""; //ignore warning of value not used.
        for (String fileStr: response.split("-----#-----")) {
          none = fileStr;
          i++;
        }
        Object[] possibilities = new Object [i];
        String [] listFiles;
        listFiles = new String [i];
        int j=0;
        for (String fileStr: response.split("-----#-----")){
          listFiles [j]=fileStr;
          possibilities [j]=fileStr;
          j++;
        }      
        
        pWriter= new PrintWriter(oStream, true);
        String choice = (String)JOptionPane.showInputDialog(
                                                            frame,
                                                            "Please choose a file to download:",
                                                            "Download",
                                                            JOptionPane.PLAIN_MESSAGE,
                                                            null,
                                                            possibilities,
                                                            listFiles[0]);
        pWriter.println(choice);
        byte[] mybytearray = new byte[1024];
        FileOutputStream fos = new FileOutputStream(choice);
        BufferedOutputStream bos = new BufferedOutputStream(fos);
        int bytesRead = iStream.read(mybytearray, 0, mybytearray.length);
        bos.write(mybytearray, 0, bytesRead);
        bos.close();
        JOptionPane.showMessageDialog(null, choice+" have been downloaded.");
      }
      
      //Client Upload
      else if (n==1) {
        /* @param dPath - Enter directory of the file
         * ex.) C:/Users/UserName/Documents
         */ 
        String dPath = /*FILE DIRECTORY*/;
        File fileDirectory = new File(dPath);
        File[] files = fileDirectory.listFiles();
        int length = fileDirectory.listFiles().length;
        Object[] possibilities = new Object [length];
        String [] listFiles = new String [length];
        if (files.length == 0) {
          System.out.println("This directory is empty");
        } else {
          int w = 0;
          for (File aFile : files) {
            listFiles [w]= aFile.getName();
            possibilities [w] = aFile.getName();
            w++;
          }
        }
        String choice = (String)JOptionPane.showInputDialog(frame,
                                                            "Please choose a file to upload:",
                                                            "Upload",
                                                            JOptionPane.PLAIN_MESSAGE,
                                                            null,
                                                            possibilities,
                                                            listFiles[0]);
        //Test.txt, revise it!
        pWriter= new PrintWriter(oStream, true);
        pWriter.println("?"+choice);
        File myFile = new File(dPath+"/"+choice);
        
        byte[] mybytearray = new byte[(int) myFile.length()];
        BufferedInputStream bInputStream = new BufferedInputStream(new FileInputStream(myFile));
        bInputStream.read(mybytearray, 0, mybytearray.length);
        oStream.write(mybytearray, 0, mybytearray.length);
        oStream.flush();
        bInputStream.close();
      }
      
      else {
        exit(oStream, pWriter);
        start = false;
        System.out.println("Closed.");
      }
    }
    
    clientSocket.close();
    iStream.close();
    oStream.close();
  }
  public static void exit (OutputStream oStream, PrintWriter pWriter) {
    pWriter= new PrintWriter(oStream, true);
    pWriter.println("#?#");
  }
}