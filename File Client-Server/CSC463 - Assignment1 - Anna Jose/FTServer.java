/* Name:  Anna Marietta Jose
 * Course:   463
 * Assignment:   #1
 */

import java.io.*;
import java.net.*;

public class FTServer implements Runnable {
  BufferedReader is;
  PrintStream os;
  Socket clientSocket;
  int id;
  FTThread server;
  OutputStream oStream = null;
  InputStream iStream = null;
  
  public FTServer(Socket clientSocket, int id, FTThread server) {
    this.clientSocket = clientSocket;
    this.id = id;
    this.server = server;
    
    System.out.println("Connection " + id + " established with: " + clientSocket );
    try {
      is = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
      os = new PrintStream(clientSocket.getOutputStream());
      oStream = clientSocket.getOutputStream();
      iStream = clientSocket.getInputStream();
    } catch (IOException e) {
      System.out.println(e);
    }
  }
  
  public void run() {
    boolean start = true;
    try {
      /* @param dPath - Enter directory of the file
         * ex.) "C:/Users/UserName/Documents"
         */ 
      String dPath = /*FILE DIRECTORY*/;
      System.out.println("Chosen Directory: "+ dPath);
      String line ="";
      File fileDirectory = new File(dPath);
      File[] files = fileDirectory.listFiles();
      if (files.length == 0) {
        System.out.println("This directory is empty");
      } 
      else {
        for (File aFile : files) {
          line = line + aFile.getName() + "-----#-----";
          //System.out.println(aFile.getName() + " - " + aFile.length());
        }
      }
      System.out.println("Server is ready for downloads from this directory");
      
      while (start == true) {
        //Enter Folder for sharing
        os.println(line);
        String myFileStr = is.readLine();
        
        if (myFileStr.charAt(0)=='?') {
          String upload = myFileStr.replace("?", "");
          File myFile = new File(upload);
          System.out.println("Uploading "+upload);
          byte[] mybytearray = new byte[1024];
          FileOutputStream fos = new FileOutputStream(myFile);
          BufferedOutputStream bos = new BufferedOutputStream(fos);
          int bytesRead = iStream.read(mybytearray, 0, mybytearray.length);
          bos.write(mybytearray, 0, bytesRead);
          bos.close();
          System.out.println("The upload was successful!");
        }
        else if(myFileStr.charAt(1)=='?' || myFileStr == "" ) {
          start = false;
          break;
        }
        else {
          System.out.println("Client is downloading "+myFileStr);
          File myFile = new File(dPath+"/"+myFileStr);
          byte[] mybytearray = new byte[(int) myFile.length()];
          BufferedInputStream bInputStream = new BufferedInputStream(new FileInputStream(myFile));
          bInputStream.read(mybytearray, 0, mybytearray.length);
          oStream.write(mybytearray, 0, mybytearray.length);
          oStream.flush();
          bInputStream.close();
        }
      }
      System.out.println( "Connection " + id + " closed." );
      oStream.close();
      iStream.close();
      is.close();
      os.close();
      clientSocket.close();
    }
    catch (IOException e) {
      System.out.println(e);
    }
  }
}