/*  Name:			Anna Marietta Jose 
 	Course:			463
 	Assignment:	  	#2
  */

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.XMLEvent;

public class TechnologyParser {
	final URL url;

	public TechnologyParser (String url) throws MalformedURLException {
		this.url = new URL(url);
	}

	public Technology read () throws IOException {
		Technology message = null;
		try {
			boolean isFeedHeader = true;
			String title = "";
			String link = "";
			String pubDate = "";
			XMLInputFactory inputFactory = XMLInputFactory.newInstance();
			InputStream in = url.openStream();
			XMLEventReader eventReader = inputFactory.createXMLEventReader(in);
			while (eventReader.hasNext()) {
				XMLEvent event = eventReader.nextEvent();
				if (event.isStartElement()) {
					String localPart = event.asStartElement().getName()
							.getLocalPart();
					switch (localPart) {
					case "item":
						if (isFeedHeader) {
							isFeedHeader = false;
							message = new Technology(title, link, pubDate);
						}
						event = eventReader.nextEvent();
						break;
					case "title":
						title = getCharacterData(event, eventReader);
						break;
					case "link":
						link = getCharacterData(event, eventReader);
						break;
					case "pubDate":
						pubDate = getCharacterData(event, eventReader);
						break;
					}
				} else if (event.isEndElement()) {
					if (event.asEndElement().getName().getLocalPart() == ("item")) {
						TechFeed technology = new TechFeed();
						technology.setLink(link);
						technology.setTitle(title);
						technology.setPubDate(pubDate);
						message.getMessages().add(technology);
						event = eventReader.nextEvent();
						continue;
					}
				}
			}
		} catch (XMLStreamException e) {
			throw new RuntimeException(e);
		}
		return message;
	}

	private String getCharacterData(XMLEvent event, XMLEventReader eventReader)
			throws XMLStreamException {
		String result = "";
		event = eventReader.nextEvent();
		if (event instanceof Characters) {
			result = event.asCharacters().getData();
		}
		return result;
	}
}
