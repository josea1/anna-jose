/*  Name:			Anna Marietta Jose 
 	Course:			463
 	Assignment:	  	#2
  */

import java.util.ArrayList;
import java.util.List;

public class Technology {
	private String title;
	private String link;
	private String pubDate;
	
	final ArrayList<TechFeed> list = new ArrayList<TechFeed>();
	
	public Technology () {
	}
	
	public Technology(String title, String link, String pubDate) {
		setTitle(title);
		setLink(link);
		setPubDate(pubDate);
	}

	public void setTitle (String title) {
		this.title = title;
	}
	public String getTitle () {
		return title;
	}
	
	public void setLink (String link) {
		this.link = link;
	}
	public String getLink () {
		return link;
	}
	
	public void setPubDate (String pubDate) {
		this.pubDate = pubDate;
	}
	public String getPubDate () {
		return pubDate;
	}
	
	public List<TechFeed> getMessages() {
		return list;
	}
	
	@Override
	public String toString() {
		return "title=" + title + "link=" + link + ", pubDate="	+ pubDate;
	}

}
