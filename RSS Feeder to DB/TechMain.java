/*  Name:			Anna Marietta Jose 
 	Course:			463
 	Assignment:	  	#2
  */

import java.io.IOException;

public class TechMain {
	public static void main (String [] args) {
		TechnologyDB runRSS = new TechnologyDB();
		try {
			//TechnologyDB.deleteTable("joseal");
			//TechnologyDB.deleteTable("Technology");
			TechnologyDB.createLog();
			runRSS.saveRSSFeed();
			System.out.println("Finished.");
		} catch (IOException e) {
			System.out.println(e +": Main Error");
		}
	}
	
}
