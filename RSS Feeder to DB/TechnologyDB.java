/*  Name:			Anna Marietta Jose 
 	Course:			463
 	Assignment:	  	#2
  */

import java.io.IOException;
import java.sql.DriverManager;
import org.xml.sax.helpers.DefaultHandler;

public class TechnologyDB extends DefaultHandler {

	public TechnologyDB () {
	}

	public static void createLog() throws IOException {
		String jdbcDriver = "com.mysql.jdbc.Driver";
		String protocolHeader = "jdbc:mysql://db4free.net/distapp";
		String user = "distapp";
		String password = "love2sql";
		try {  
			Class.forName(jdbcDriver);
			System.out.println("...Driver loaded");
			java.sql.Connection connection = DriverManager.getConnection(protocolHeader, user, password);
			System.out.println("...Connection established");
			java.sql.Statement query = connection.createStatement();

			try {
				TechnologySQL sql = new TechnologySQL();
				if (query.executeUpdate(sql.checkTableExists("josea1")) != -1) {
					query.executeUpdate(sql.getCreateUserTable());
					System.out.println("Created table: josea1");
				}
				query.executeUpdate(sql.getInsertUserTable());
				System.out.println("Inserted timestamp in table: joseal");
			}
			catch (Exception e) {
				System.out.println("DBJosea1: Problem accessing the log "+e);
			}
		}
		catch (Exception e) {
			System.out.println("JDBC: Problem accessing the log "+e);
		}
	}

	public void saveRSSFeed() throws IOException {
		String jdbcDriver = "com.mysql.jdbc.Driver";
		String protocolHeader = "jdbc:mysql://db4free.net/distapp";
		String user = "distapp";
		String password = "love2sql";
		try {  
			Class.forName(jdbcDriver);
			System.out.println("...Driver loaded");
			java.sql.Connection connection = DriverManager.getConnection(protocolHeader, user, password);
			System.out.println("...Connection established");
			java.sql.Statement query = connection.createStatement();

			try {
				TechnologySQL sql = new TechnologySQL();
				if (query.executeUpdate(sql.checkTableExists("Technology")) != -1) {
					query.executeUpdate(sql.getCreateTechnologyTable());
					System.out.println("Created table: Technology");
				}
				TechnologyParser parser = new TechnologyParser("http://rss.cnn.com/rss/cnn_tech.rss");
				Technology read = parser.read();
				System.out.println(read);
				for (TechFeed message : read.getMessages()) {
					query.executeUpdate(sql.insertTechnologyTable(message.getTitle(), message.getLink(), message.getPubDate()));
					System.out.println(message);
				}
			}
			catch (Exception e) {
				System.out.println("DBTechnology: Problem accessing the log "+e);
			}
		}
		catch (Exception e) {
			System.out.println("JDBC: Problem accessing the log "+e);
		}
	}

	public static void deleteTable(String table) throws IOException {
		String jdbcDriver = "com.mysql.jdbc.Driver";
		String protocolHeader = "jdbc:mysql://db4free.net/distapp";
		String user = "distapp";
		String password = "love2sql";
		try {  
			Class.forName(jdbcDriver);
			System.out.println("...Driver loaded");
			java.sql.Connection connection = DriverManager.getConnection(protocolHeader, user, password);
			System.out.println("...Connection established");
			java.sql.Statement query = connection.createStatement();
			try {
				TechnologySQL sql = new TechnologySQL();
				if (query.executeUpdate(sql.checkTableExists(table)) == -1) {
					query.executeUpdate(sql.getDeleteTechnologyTable());
					System.out.println("Deleted table: "+table);
				}
				else {
					System.out.println(table + ": does not exist in database");
				}
			}
			catch (Exception e) {
				System.out.println("Deleting table failed: "+e);
			}
		}
		catch (Exception e) {
			System.out.println("Deleting table failed: "+e);
		}
	}
}
