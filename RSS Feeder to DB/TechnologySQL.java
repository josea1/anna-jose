/*  Name:			Anna Marietta Jose 
 	Course:			463
 	Assignment:	  	#2
  */

public class TechnologySQL {
	private String createUserTable = "CREATE TABLE josea1(tableName VARCHAR(50)," +	"timestamp TIMESTAMP)";
	private String deleteUserTable = "DROP TABLE josea1";
	
	private String createTechnologyTable = "CREATE TABLE Technology(title VARCHAR(255)," +"link VARCHAR(255)," +"pubDate VARCHAR(100))";
	private String deleteTechnologyTable = "DROP TABLE Technology";
	
	private String insertUserTable = "INSERT INTO `josea1`(`tableName`, `timestamp`) VALUES ('Technology', SYSDATE())";
	
	public String insertTechnologyTable (String title, String link, String pubDate) {
		title = title.replace("'","''");
		link = link.replace("'","''");
		pubDate = pubDate.replace("'","''");
		String insert = "INSERT INTO `Technology`(`title`, `link`, `pubDate`) VALUES ('" + title + "','" + link + "','" + pubDate + "')";
		return insert;
	}
	
	public String checkTableExists (String table) {
		String show = "SHOW TABLES LIKE '"+ table +"'";
		return show;
	}
	
	public String getCreateUserTable () {
		return createUserTable;
	}
	
	public String getDeleteUserTable () {
		return deleteUserTable;
	}
	
	public String getCreateTechnologyTable () {
		return createTechnologyTable;
	}
	
	public String getDeleteTechnologyTable () {
		return deleteTechnologyTable;
	}
	
	public String getInsertUserTable () {
		return insertUserTable;
	}
}
